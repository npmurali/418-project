import random

SMALL_TREE_SIZE = 1024
MED_TREE_SIZE = 32768
LARGE_TREE_SIZE = 1048576

def createTraceFilesMix(size, fileName):
    ops = ["I", "D", "F", "S"]
    fileOutput = ""
    currNodes = []
    nodeVals = random.sample(range(0, size), size)

    for val in nodeVals:
        op = random.choice(ops)
        if (op == "S"):
            fileOutput += op + "\n"

        elif (op == "F" and currNodes != []):
            val = random.choice(currNodes)
            fileOutput += op + " " + str(val) + "\n"

        elif(op == "D" and currNodes != []):
            val = random.choice(currNodes)
            currNodes.remove(val)
            fileOutput += op + " " + str(val) + "\n"

        else:
            op = "I"
            currNodes.append(val)
            fileOutput += op + " " + str(val) + "\n"


    with open(fileName, "w+") as d:
        d.write(fileOutput[:len(fileOutput)-1])

def createTraceFilesIndiv(op, size, fileName):

    fileOutput = ""
    nodeVals = random.sample(range(0, size), size)

    for val in nodeVals:
        fileOutput += op + " " + str(val) + "\n"

    with open(fileName, "w+") as d:
        d.write(fileOutput[:len(fileOutput)-1])

if __name__== "__main__":
    # create small tree trace files
    createTraceFilesIndiv("I", SMALL_TREE_SIZE, "traces/insert_small.txt");
    createTraceFilesIndiv("F", SMALL_TREE_SIZE, "traces/search_small.txt");
    createTraceFilesIndiv("D", SMALL_TREE_SIZE, "traces/delete_small.txt");
    createTraceFilesMix(SMALL_TREE_SIZE, "traces/mixed_small.txt");

    # create medium tree trace files
    createTraceFilesIndiv("I", MED_TREE_SIZE, "traces/insert_med.txt");
    createTraceFilesIndiv("F", MED_TREE_SIZE, "traces/search_med.txt");
    createTraceFilesIndiv("D", MED_TREE_SIZE, "traces/delete_med.txt");
    createTraceFilesMix(MED_TREE_SIZE, "traces/mixed_med.txt");

    # create large tree trace files
    createTraceFilesIndiv("I", LARGE_TREE_SIZE, "traces/insert_large.txt");
    createTraceFilesIndiv("F", LARGE_TREE_SIZE, "traces/search_large.txt");
    createTraceFilesIndiv("D", LARGE_TREE_SIZE, "traces/delete_large.txt");
    createTraceFilesMix(LARGE_TREE_SIZE, "traces/mixed_large.txt");
