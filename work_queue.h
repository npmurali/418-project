/*
 * Work queue data type
 */
#include <pthread.h>

typedef enum wq_op_t {I, D, F, S} wq_op_t;

typedef struct wq_item_t {
    wq_op_t operation;
    int value;
    struct wq_item_t *next;
} wq_item_t;

typedef struct wq_t {
    wq_item_t *head;
    wq_item_t *tail;
    int size;
    pthread_mutex_t queue_lock;
} wq_t;


void wq_init( wq_t *queue );

int wq_enq( wq_t *queue, wq_op_t op, int value );

int wq_deq( wq_t *queue, wq_item_t *item );

void wq_destroy( wq_t *queue );

int wq_is_empty( wq_t *queue );

void print_wq( wq_t *queue );







