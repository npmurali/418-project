/**
 * Testing framework
 */

#include "seq_BST.h"
#include "fg_BST.h"
#include "lf_BST.h"
#include "work_queue.h"
#include "hash_set.h"
#include "cycletimer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef enum trace_op_num_t {SMALL, MEDIUM, LARGE, ALL} trace_op_num_t;
typedef enum trace_op_t {INDIV, MIX, BOTH} trace_op_t;
typedef enum bst_imp_t {SEQ, FG, LF} bst_imp_t;

#define INDIV_START (0)
#define MIXED_START (3)
#define NUM_FILES (4)
#define LINE_SIZE (10)
#define NUM_THREADS (12)

bst_imp_t implementation_type = SEQ;
trace_op_num_t trace_op_num = SMALL;
int correctness_check = 0;

trace_op_t trace_op = BOTH;
seq_node_t *seq_BST = NULL;
fg_tree_t *fg_tree_top = NULL;
fg_node_t *fg_BST = NULL;
lf_node_t *lf_BST;

hash_set_t seq_set;
hash_set_t fg_set;
hash_set_t lf_set;

typedef struct thread_data {
    wq_t *work_queue;
    int result;
} thread_data;

char *small_files[NUM_FILES] = {
    //"test.txt"
    "traces/insert_small.txt",
    "traces/search_small.txt",
    "traces/delete_small.txt",
    //"traces/mixed_small.txt"
};

char *med_files[NUM_FILES] = {
    "traces/insert_med.txt",
    "traces/search_med.txt",
    //"traces/delete_med.txt",
    //"traces/mixed_med.txt"
};

char *large_files[NUM_FILES] = {
    "traces/insert_large.txt",
    "traces/search_large.txt",
    //"traces/delete_large.txt",
    //"traces/mixed_large.txt"
};

static void usage (void) {
    char *use_string = "[-t TFILE] [-i (f|l)] [-o (r|w|m)]";
    fprintf(stderr, "Usage: %s\n", use_string);
    fprintf(stderr, "   -h        Print this message\n");
    fprintf(stderr, "   -i IMP    Implementation Type:\n");
    fprintf(stderr, "             f: Fine-grained.  Runs only the fine-grained implementation on all or operation type specified\n");
    fprintf(stderr, "             l: Lock-free.  Runs only the lock-free implementation on all or operation type specified\n");
    fprintf(stderr, "   -o OPNUM  Number of Operations:\n");
    fprintf(stderr, "             s: 2^10 operations\n");
    fprintf(stderr, "             m: 2^15 operations\n");
    fprintf(stderr, "             l: 2^20 operations\n");
    fprintf(stderr, "   -t OPTYPE Type of Operations:\n");
    fprintf(stderr, "             i: individial operations\n");
    fprintf(stderr, "             m: mix of all 4 operations\n");
    fprintf(stderr, "             b: individual and mix of all 4 operations\n");
    fprintf(stderr, "   -c        Correctness checked\n");
    exit(0);
}

void parse_cmdline(int argc, char *argv[]) {
    int c;
    char *optstring = "ht:i:o:c";

    while ((c = getopt(argc, argv, optstring)) != -1) {
        switch (c) {
            case 'h':
                usage();
                break;
            case 'i':
                if (optarg[0] == 'f') {
                    implementation_type = FG;
                }
                else if (optarg[0] == 'l') {
                    implementation_type = LF;
                }
                else {
                    fprintf(stderr, "Invalid implementation type '%c'\n", optarg[0]);
                    usage();
                    exit(1);
                }
                break;
            case 'o':
                if (optarg[0] == 's') {
                    trace_op_num = SMALL;
                } else if (optarg[0] == 'm') {
                    trace_op_num = MEDIUM;
                } else if (optarg[0] == 'l') {
                    trace_op_num = LARGE;
                }
                else {
                    fprintf(stderr, "Invalid number of operations'%c'\n", optarg[0]);
                    usage();
                    exit(1);
                }
                break;
            case 't':
                if (optarg[0] == 'i') {
                    trace_op = INDIV;
                } else if (optarg[0] == 'm') {
                    trace_op = MIX;
                } else if (optarg[0] == 'b') {
                    trace_op = BOTH;
                }
                else {
                    fprintf(stderr, "Invalid operation type'%c'\n", optarg[0]);
                    usage();
                    exit(1);
                }
                break;
            case 'c':
                correctness_check = 1;
                break;
            default:
                break;
        }
    }

}


int build_wq( char *trace_file, wq_t *work_queue ) {
    FILE *tfile;
    char line[LINE_SIZE];
    int invalid_arg = 0;

    tfile = fopen(trace_file, "r");
    if (tfile == 0) {
        fprintf(stderr, "cannot open file %s\n", trace_file);
        return -1;
    }

    if (work_queue == NULL) {
        work_queue = (wq_t *)malloc(sizeof(wq_t));
        if (work_queue == NULL) {
            return -1;
        }
    }
    wq_init(work_queue);

    // parse file line by line and put each operation,value into queue
    while (fgets(line, sizeof(line), tfile)) {

        wq_op_t op;
        int value = -1;
        invalid_arg = 0;
        switch(line[0]) {
            case('I'):
                op = I;
                value = atoi(&(line[2]));
                invalid_arg = 0;
                break;
            case('D'):
                op = D;
                value = atoi(&(line[2]));
                invalid_arg = 0;
                break;
            case('F'):
                op = F;
                value = atoi(&(line[2]));
                invalid_arg = 0;
                break;
            case('S'):
                op = S;
                break;
            default:
                invalid_arg = 1;
                break;
        }
        if (!invalid_arg) {
            pthread_mutex_lock(&work_queue->queue_lock);
            if (wq_enq(work_queue, op, value) < 0) {
                pthread_mutex_unlock(&work_queue->queue_lock);
                return -1;
            }
            pthread_mutex_unlock(&work_queue->queue_lock);
        }
    }
    fclose(tfile);
    return 0;
}

void *thread_fn_fg( void *arg ) {
    thread_data *t_data = (thread_data *)arg;
    wq_t *work_queue = t_data->work_queue;
    pthread_mutex_lock(&work_queue->queue_lock);
    while (!wq_is_empty(work_queue)) {
        wq_item_t *item = (wq_item_t *)malloc(sizeof(wq_item_t));
        wq_deq(work_queue, item);
        pthread_mutex_unlock(&work_queue->queue_lock);
        switch(item->operation) {
            case (I):
                //fg_tree_top->root = fg_BST_insert(fg_tree_top, fg_tree_top->root, NULL, item->value);
                fg_tree_top->root = fg_BST_insert(fg_tree_top->root, NULL, item->value);
                //fg_BST = fg_BST_insert(fg_BST, NULL, item->value);

                break;
            case (D):
                //fg_BST_delete(fg_tree_top, item->value);
                fg_tree_top->root = fg_BST_delete(fg_tree_top->root, NULL, item->value);
                //fg_BST = fg_BST_delete(fg_BST, NULL, item->value);
                break;
            case(F):
                //if (fg_BST_search(fg_tree_top->root, NULL, item->value) == NULL) {
                if (fg_BST_search(fg_tree_top->root, NULL, item->value) == NULL) {
                //if (fg_BST_search(fg_BST, NULL, item->value) == NULL) {
                    //printf("FG: Not found\n");
                }
                else {
                    //printf("FG: Found\n");
                }
                break;

            case(S):
                //printf("FG: sum = %d\n", fg_BST_sum(fg_tree_top->root, NULL));
                printf("FG: sum = %d\n", fg_BST_sum(fg_tree_top->root, NULL));
                //printf("FG: sum = %d\n", fg_BST_sum(fg_BST, NULL));
                break;
            default:
                break;
        }
        if (correctness_check) {
            //if (!fg_is_BST(fg_tree_top->root)) {
            if (!fg_is_BST(fg_BST)) {
                t_data->result = -1;
            }
        }
        free(item);
        pthread_mutex_lock(&work_queue->queue_lock);
    }
    pthread_mutex_unlock(&work_queue->queue_lock);
    t_data->result = 0;
    return NULL;
}

void *thread_fn_lf( void *arg ) {
    thread_data *t_data = (thread_data *)arg;
    wq_t *work_queue = t_data->work_queue;
    while (!wq_is_empty(work_queue)) {
        wq_item_t *item = (wq_item_t *)malloc(sizeof(wq_item_t));
        wq_deq(work_queue, item);
        switch(item->operation) {
            case (I):
                lf_BST_insert(lf_BST, item->value);
                break;
            case (D):
                lf_BST_delete(lf_BST, item->value);
                break;
            case(F):
                if (lf_BST_search(lf_BST, item->value) == 0) {
                    printf("LF: Not found\n");
                }
                else {
                    printf("LF: Found\n");
                }
                break;
            case(S):
                printf("LF: sum = %d\n", lf_BST_sum(lf_BST));
                break;
            default:
                break;
        }
        if (correctness_check) {
            if (!lf_is_BST(lf_BST)) {
                t_data->result = -1;
            }
        }
        free(item);
    }
    t_data->result = 0;
    return NULL;
}


int perform_work_fg( wq_t *work_queue ) {
    pthread_t tids[NUM_THREADS];
    pthread_mutex_init(&tree_lock, NULL);
    int tid;
    int valid;
    thread_data t_data[NUM_THREADS];

    for (tid = 0; tid < NUM_THREADS; tid++) {
        thread_data cur_data = t_data[tid];
        cur_data.work_queue = work_queue;

        if (pthread_create(&tids[tid], NULL, &thread_fn_fg, (void *)&cur_data) < 0) {
            return -1;
        }
    }

    for (tid = 0; tid < NUM_THREADS; tid++) {
        if (pthread_join(tids[tid], NULL) < 0) {
            return -1;
        }
        // thread returned invalid bst
        valid = t_data[tid].result;
        if (valid == -1) {
            return -1;
        }
    }

    // create hash set from fg tree
    if (correctness_check) {
        //fg_bst_to_hash_set(fg_tree_top->root, &fg_set);
        fg_bst_to_hash_set(fg_BST, &fg_set);

        if (hash_set_compare(&fg_set, &seq_set) == 0) {
            return -1;
        }
    }

    return 0;
}

int perform_work_lf( wq_t *work_queue ) {
    pthread_t tids[NUM_THREADS];
    int tid;
    int valid;
    thread_data t_data[NUM_THREADS];



    for (tid = 0; tid < NUM_THREADS; tid++) {
        thread_data cur_data = t_data[tid];
        cur_data.work_queue = work_queue;

        if (pthread_create(&tids[tid], NULL, &thread_fn_lf, (void *)&cur_data) < 0) {
            return -1;
        }
    }

    for (tid = 0; tid < NUM_THREADS; tid++) {
        if (pthread_join(tids[tid], NULL) < 0) {
            return -1;
        }
        // thread returned invalid bst
        valid = t_data[tid].result;
        if (valid == -1) {
            return -1;
        }
    }

    // convert tree to hash set after all work performed
    if (correctness_check) {
        lf_bst_to_hash_set(lf_BST, &lf_set);

        if (hash_set_compare(&lf_set, &seq_set) == 0) {
           return -1;
        }
    }
    return 0;
}


int perform_work_seq( wq_t *work_queue ) {

    // iterate through operations from trace file
    while(!wq_is_empty(work_queue)) {
        wq_item_t *item = malloc(sizeof(wq_item_t));
        wq_deq(work_queue, item);

        // perform the operation
        switch(item->operation) {
            case(I):
                seq_BST = seq_BST_insert(seq_BST, item->value);
                break;
            case(D):
                seq_BST = seq_BST_delete(seq_BST, item->value);
                break;

            case(F):
                if (seq_BST_search(seq_BST, item->value) == NULL) {
                    printf("SEQ: Not found\n");
                }
                else {
                    printf("SEQ: Found\n");
                }
                break;
            case(S):
                printf("SEQ: sum = %d\n", seq_BST_sum(seq_BST));
                break;
            default:
                break;
        }
        free(item);
        if (!seq_is_BST(seq_BST)) {
            printf("bad bst\n");
            return -1;
        }
    }
    // convert tree to hash set after all work performed
    seq_bst_to_hash_set(seq_BST, &seq_set);
    return 0;
}

void run_trace_file(char *trace_file) {
    wq_t *par_queue = NULL;
    wq_t *seq_queue = NULL;
    double start;
    double end;

    seq_queue = (wq_t *)malloc(sizeof(wq_t));
    if (build_wq(trace_file, seq_queue) < 0) {
        free(seq_queue);
        return;
    }

    if (implementation_type != SEQ) {
        par_queue = (wq_t *)malloc(sizeof(wq_t));
        if (build_wq(trace_file, par_queue) < 0) {
            free(par_queue);
            return;
        }
    }

    // perform sequential work
    start = currentSeconds();
    if (perform_work_seq(seq_queue) < 0) {
        wq_destroy(seq_queue);
        free(seq_queue);
        return;
    }
    end = currentSeconds();
    if (!correctness_check) {
        printf("%s trace file on SEQ implementation: %f\n", trace_file, end-start);
    }

    switch (implementation_type) {
        case (FG):
            start = currentSeconds();
            if (perform_work_fg(par_queue) < 0) {
                printf("invalid BST from FG implementation\n");
                wq_destroy(par_queue);
                free(par_queue);
                return;
            }
            end = currentSeconds();
            if (!correctness_check) {
                printf("%s trace file on FG implementation: %f\n", trace_file, end-start);
                //print_fg_bst(fg_tree_top->root);
                print_fg_bst(fg_BST);
            }
            break;
        case (LF):
            start = currentSeconds();
            if (perform_work_lf(par_queue) < 0) {
                printf("invalid BST from LF implementation\n");
                wq_destroy(par_queue);
                free(par_queue);
                return;
            }
            end = currentSeconds();
            printf("LF passed correctness test\n");
            if (!correctness_check) {
                printf("%s trace file on LF implementation: %f\n", trace_file, end-start);
            }
            //printf("LF BST\n");
            //print_lf_bst(lf_BST);
            break;
        default:

            break;
    }
    wq_destroy(seq_queue);
    free(seq_queue);

    if (implementation_type == FG || implementation_type == LF) {
        wq_destroy(par_queue);
        free(par_queue);
    }
}

int main(int argc, char *argv[]) {

    int i;
    parse_cmdline(argc, argv);
    fg_tree_top = malloc(sizeof(fg_tree_t));
    fg_tree_top->root = NULL;
    pthread_mutex_init(&(fg_tree_top->tree_lock), NULL);

    lf_BST = lf_BST_init();

    // initialize hash sets
    if (hash_set_init(&seq_set) < 0) {
        return -1;
    }
    if (hash_set_init(&fg_set) < 0) {
        return -1;
    }
    if (hash_set_init(&lf_set) < 0) {
        return -1;
    }



    switch(trace_op_num){
        case(SMALL):
            if (trace_op != MIX){
                for (i = INDIV_START; i < MIXED_START; i++) {
                    run_trace_file(small_files[i]);
                }
            }
            if (trace_op != INDIV) {
                for (i = MIXED_START; i < NUM_FILES; i++) {
                    run_trace_file(small_files[i]);
                }
            }
            break;

        case(MEDIUM):
            if (trace_op != MIX){
                for (i = INDIV_START; i < MIXED_START; i++) {
                    run_trace_file(med_files[i]);
                }
            }
            if (trace_op != INDIV) {
                for (i = MIXED_START; i < NUM_FILES; i++) {
                    run_trace_file(med_files[i]);
                }
            }
            break;

        case(LARGE):
            if (trace_op != MIX){
                for (i = INDIV_START; i < MIXED_START; i++) {
                    run_trace_file(large_files[i]);
                }
            }
            if (trace_op != INDIV) {
                for (i = MIXED_START; i < NUM_FILES; i++) {
                    run_trace_file(large_files[i]);
                }
            }
            break;

        case(ALL):
            if (trace_op != MIX){
                for (i = INDIV_START; i < MIXED_START; i++) {
                    run_trace_file(small_files[i]);
                }
                for (i = INDIV_START; i < MIXED_START; i++) {
                    run_trace_file(med_files[i]);
                }
                for (i = INDIV_START; i < MIXED_START; i++) {
                    run_trace_file(large_files[i]);
                }
            }
            if (trace_op != INDIV) {
                for (i = MIXED_START; i < NUM_FILES; i++) {
                    run_trace_file(small_files[i]);
                }
                for (i = MIXED_START; i < NUM_FILES; i++) {
                    run_trace_file(med_files[i]);
                }
                for (i = MIXED_START; i < NUM_FILES; i++) {
                    run_trace_file(large_files[i]);
                }
            }
            break;
    }
    return 0;
}
