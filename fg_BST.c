/*
 * Fine-grained Locking Concurrent BST implementation
 */
#include "fg_BST.h"
#include <stdio.h>
#include <limits.h>
#include <unistd.h>

// assume that parent is locked
fg_node_t *fg_BST_insert( fg_node_t *root, fg_node_t *parent, int value ) {

    // empty tree
    if (root == NULL && parent == NULL) {
        pthread_mutex_lock(&tree_lock);
        if (tree_top == NULL) {
            tree_top = (fg_node_t *)malloc(sizeof(fg_node_t));
            tree_top->left = NULL;
            tree_top->right = NULL;
            tree_top->value = value;
            pthread_mutex_init(&tree_top->node_lock, NULL);
            pthread_mutex_unlock(&tree_lock);
            return tree_top;
        }
        root = tree_top;
        pthread_mutex_unlock(&tree_lock);
    }

    else if (root == NULL) {
        root = (fg_node_t *)malloc(sizeof(fg_node_t));
        root->left = NULL;
        root->right = NULL;
        root->value = value;
        pthread_mutex_init(&root->node_lock, NULL);
        if (parent != NULL) {
            pthread_mutex_unlock(&parent->node_lock);
        }
        return root;
    }
    // lock root
    pthread_mutex_lock(&root->node_lock);
    if (parent != NULL) {
        // unlock your parent before traversing subtree
        pthread_mutex_unlock(&parent->node_lock);
    }

    // insert into left subtree
    if (value < root->value) {
        root->left = fg_BST_insert(root->left, root, value);
    }
    // insert into right subtree
    else if (value > root->value) {
        root->right = fg_BST_insert(root->right, root, value);
    }
    return root;
}

// there is no need to assign tree top here, since if
// a null pointer is passed into search, it will return NULL
// regardless of if another thread has modified the root in between
fg_node_t *fg_BST_search( fg_node_t *root, fg_node_t *parent, int value ) {
    // empty tree or value at root
    if (root == NULL || root->value == value) {
        if (parent != NULL) {
            pthread_mutex_unlock(&parent->node_lock);
        }
        return root;
    }
    pthread_mutex_lock(&root->node_lock);

    if (parent != NULL) {
        pthread_mutex_unlock(&parent->node_lock);
    }
    // search left subtree
    if (value < root->value) {
        return fg_BST_search(root->left, root, value);
    }
    // search right subtree
    return fg_BST_search(root->right, root, value);
}

// assume on entry this function holds the parent's node lock
fg_node_t *fg_BST_delete( fg_node_t *root, fg_node_t *parent, int value ) {

    // empty tree or leaf node
    if (root == NULL) {
        pthread_mutex_lock(&tree_lock);
        if (parent != NULL) {
            pthread_mutex_unlock(&parent->node_lock);
        }
        pthread_mutex_unlock(&tree_lock);
        return root;
    }

    pthread_mutex_lock(&root->node_lock);
    if (value < root->value) {
        if (parent != NULL) {
            pthread_mutex_unlock(&parent->node_lock);
        }

        root->left = fg_BST_delete(root->left, root, value);
    }
    else if (value > root->value) {
        if (parent != NULL) {
            pthread_mutex_unlock(&parent->node_lock);
        }
        root->right = fg_BST_delete(root->right, root, value);
    }
    else {

        if (root->left == NULL) {
            fg_node_t *temp = root;
            root = root->right;
            if (parent != NULL) {
                pthread_mutex_unlock(&parent->node_lock);
            }
            pthread_mutex_unlock(&temp->node_lock);
            return root;
        }
        else if (root->right == NULL) {
            fg_node_t *temp = root;
            root = root->left;
            if (parent != NULL) {
                pthread_mutex_unlock(&parent->node_lock);
            }
            pthread_mutex_unlock(&temp->node_lock);
            return root;
        }
        else {
            if (parent != NULL) {
                pthread_mutex_unlock(&parent->node_lock);
            }
            fg_node_t *temp = root->right;
            pthread_mutex_lock(&temp->node_lock);

            while (temp->left != NULL) {
                pthread_mutex_lock(&temp->left->node_lock);
                pthread_mutex_unlock(&temp->node_lock);
                temp = temp->left;
            }
            root->value = temp->value;
            pthread_mutex_unlock(&temp->node_lock);

            root->right = fg_BST_delete(root->right, root, temp->value);
        }
    }
    return root;
}


// called with alock on the root
fg_node_t *fg_delete_root_node(fg_node_t *node) {
    if (node == NULL) {
        return node;
    }

    if (node->left == NULL) {
        return node->right;
    }

    else if (node->right == NULL) {
        return node->left;
    }

    fg_node_t *temp = node->right;
    fg_node_t *parent = NULL;

    while (temp->left != NULL) {
        parent = temp;
        temp = temp->left;
    }
    node->value = temp->value;

    if (node->right != temp) {
        parent->left = temp->right;
    }
    else {
        node->right = temp->right;
    }

    return node;
}

/*void fg_BST_delete( fg_tree_t *fg_tree_top,  int value ) {
    pthread_mutex_lock(&(fg_tree_top->tree_lock));

    //printf("before deleting %d...\n", value);
    //print_fg_bst(fg_tree_top->root);

    fg_node_t *node, *parent, *replacement;
    node = fg_tree_top->root;
    parent = NULL;

    while (node != NULL && node->value != value) {
        parent = node;
        if (value < node->value) {
            node = node->left;
        }
        else if (value > node->value) {
            node = node->right;
        }
    }

    replacement = fg_delete_root_node(node);

    if (parent == NULL) {
        fg_tree_top->root = replacement;

        //printf("after deleting %d...\n", value);
        //print_fg_bst(fg_tree_top->root);
        //printf("done!\n\n");

        pthread_mutex_unlock(&(fg_tree_top->tree_lock));
        return;
    }
    if (parent->left == node) {
        parent->left = replacement;
    }
    else {
        parent->right = replacement;
    }

    //printf("after deleting %d...\n", value);
    //print_fg_bst(fg_tree_top->root);
    //printf("done!\n\n");

    pthread_mutex_unlock(&(fg_tree_top->tree_lock));
    return;
}
*/
int fg_BST_sum( fg_node_t *root, fg_node_t *parent ) {
    // empty tree
    if (root == NULL) {
        if (parent != NULL) {
            pthread_mutex_unlock(&parent->node_lock);
        }
        return 0;
    }
    pthread_mutex_lock(&root->node_lock);
    if (parent != NULL) {
        pthread_mutex_unlock(&parent->node_lock);
    }
    return (root->value + fg_BST_sum(root->left, root) + fg_BST_sum(root->right, root));
}

int fg_is_BST_helper( fg_node_t *root, int min, int max ) {
    // emptry tree is valid
    if (root == NULL) {
        return 1;
    }

    if (root->value < min || root->value > max) {
        return 0;
    }

    // recursive check tightening constraints
    return (fg_is_BST_helper(root->left, min, root->value - 1)) &&
           (fg_is_BST_helper(root->right, root->value + 1, max));
}
int fg_is_BST( fg_node_t *root ) {
    return fg_is_BST_helper(root, INT_MIN, INT_MAX);
}

void print_fg_bst( fg_node_t *root ) {

    if (root != NULL) {
        printf("%d \n", root->value);
        print_fg_bst(root->left);
        print_fg_bst(root->right);
    }
}

void fg_bst_to_hash_set( fg_node_t *root, hash_set_t *set ) {
    if (hash_set_reinit(set) < 0) {
        return;
    }

    if (root != NULL) {
        hash_set_insert(set, root->value);
        fg_bst_to_hash_set(root->left, set);
        fg_bst_to_hash_set(root->right, set);
    }
    return;
}
