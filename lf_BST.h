#ifndef __LF_BST_H
#define __LF_BST_H
/**
 * Lock free Binary search tree
 *
 */
#include "hash_set.h"

/*
 * Node data structure
 */
typedef struct lf_node_t {
    struct lf_node_t *left;
    struct lf_node_t *right;
    int value;
} lf_node_t;

/*
 * Initialize BST with sentinel nodes
 */
lf_node_t *lf_BST_init( void );

/*
 * Insert operation
 */
int lf_BST_insert( lf_node_t *root, int value );

/*
 * Search operation
 */
int lf_BST_search( lf_node_t *root, int value );

/*
 * Delete operation
 */
int lf_BST_delete( lf_node_t *root, int value );

/*
 * Sum operation
 */
int lf_BST_sum( lf_node_t *root );

/**
 * Validator
 */
int lf_is_BST( lf_node_t *root );

/**
 * Debugging print function
 */
void print_lf_bst( lf_node_t *root );

void lf_bst_to_hash_set( lf_node_t *root, hash_set_t *set );
#endif
