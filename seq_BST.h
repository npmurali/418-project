#ifndef __SEQ_BST_H
#define __SEQ_BST_H
/**
 * Sequential Binary search tree
 *
 * Authors: Tushita Gupta and Namrita Murali
 */

#include "hash_set.h"

/*
 * Node data structure
 */
typedef struct seq_node_t {
    struct seq_node_t *left;
    struct seq_node_t *right;
    int value;
} seq_node_t;

/*
 * Insert operation
 */
seq_node_t *seq_BST_insert( seq_node_t *root, int value );

/*
 * Search operation
 */
seq_node_t *seq_BST_search( seq_node_t *root, int value );

/*
 * Delete operation
 */
seq_node_t *seq_BST_delete( seq_node_t *root, int value );

/*
 * Sum operation
 */
int seq_BST_sum( seq_node_t *root );

/**
 * Validator
 */
int seq_is_BST( seq_node_t *root );

/**
 * Debugging print function
 */
void print_seq_bst( seq_node_t *root );

/**
 * BST --> hash set
 */
void seq_bst_to_hash_set( seq_node_t *root, hash_set_t *set );

#endif
