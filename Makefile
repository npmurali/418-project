CC=gcc
CFLAGS=-g -O3 -Wall -DDEBUG=$(DEBUG)
LDFLAGS=-lpthread
TARGET=test
CFILES= seq_BST.c fg_BST.c lf_BST.c benchmark.c work_queue.c cycletimer.c hash_set.c

HFILES= seq_BST.h fg_BST.h lf_BST.h work_queue.h cycletimer.h hash_set.h
all: test

test: $(CFILES) $(HFILES)
	$(CC) $(CFLAGS) -o test $(CFILES) $(LDFLAGS)

clean:
	rm test
