/**
 * @file hash_set.h
 * 
 * @brief Library for a generic hash_setionary / hash table implementation. 
 * 
 * @author Namrita Murali
 * 
 * @bugs No known bugs. 
 */
#ifndef __HASH_SET_H
#define __HASH_SET_H

#define SET_ARR_SIZE (3)
#define MAX_LOAD_FACTOR (1)

/** @brief linked element in the hash_setionary **/
typedef struct elt_t{
    int key;
    struct elt_t* next;
} elt_t;

/** @brief hash_setionary structure **/
typedef struct {
    int size;
    int n;
    elt_t **table;
} hash_set_t;

/** @brief hash_set_init
 *  @param destroy_fn
 *  Allocates space for and initializes the hash_setionary **/
int hash_set_init( hash_set_t *d );

int hash_set_reinit( hash_set_t *d );

/** @brief hash_set_destroy
 *  @param d : hash_setionary that is to be destroyed
 *  This function frees the entire structure but also uses destroy_fn
 *  to deal with whether we free the values stored in the hash_setionary or not**/
int hash_set_destroy( hash_set_t *d );

/** @brief hash_set_insert
 *  @param d : hash_setionary the function inserts the key value pair into
 *  @param key
 *  @param value
 *  This function uses the hash_function to hash the key, and
 *  inserts the value into the hash_setionary */
int hash_set_insert( hash_set_t *d, int key );

/** @brief hash_set_delete
 *  @param d : hash_setionary the function deletes the key value pair from
 *  @param key
 *  This function deletes the key value pair from the hash_setionary and destroys
 *  the value by calling destroy_fn(value)
 *  @returns 0 if it succesfully deleted the elelment, -1 if there was
 *  no such element
 */
int hash_set_delete( hash_set_t *d, int key );

/** @brief hash_set_search
 *  @param d : hash_setionary the function searches for the key value pair in
 *  @param key
 *  @returns value if theres an entry that matches the key, NULL otherwise */
int hash_set_search( hash_set_t *d, int key );

/** @brief hash_set_copy
 *  @param d1 : hash_setionary to copy entries from
 *  @param d2 : hash_setionary to copy entries to
 *  @returns 0 if it succesfully copied the hash_setionary -1 if there was an error
 */
int hash_set_copy(hash_set_t *d1, hash_set_t *d2);

int hash_set_compare( hash_set_t *d1, hash_set_t *d2 );

#endif
