
#include "fg_BST.h"
#include <pthread.h>
#include <stdio.h>

#define NUM_THREADS (3)
int values[] = {6, 4, 8, 18, 15, 10, 11, 12, 13, 14, 16, 34, 35};
int search_values[] = {7, 6, 15, 3};
int delete_values[] = {18, 8, 6, 4, 10, 15, 35, 34, 14, 16, 13, 11, 12};
fg_node_t *root = NULL;

void *thread_fn( void *arg ) {
    int tid = *(int *)arg;
    root = fg_BST_insert(root, NULL, values[tid]);
    return NULL;
}

void *search_thread_fn( void *arg ) {
    int tid = *(int *)arg;
    printf("searching for: %d...\n", search_values[tid]);
    printf("found: %p\n", fg_BST_search(root, NULL, search_values[tid]));
    return NULL;
}

void *delete_thread_fn( void *arg ) {
    int tid = *(int *)arg;
    fg_BST_delete(root, NULL, delete_values[tid]);
    return NULL;
}

void *sum_thread_fn( void *arg ) {
    printf("sum: %d\n", fg_BST_sum(root, NULL));
    return NULL;
}

int main(void) {

    pthread_t thread0, thread1, thread2, thread3, thread4, thread5;
    int t0 = 0;
    int t1 = 1;
    int t2 = 2;
    int t3 = 3;
    int t4 = 4;
    int t5 = 5;

    pthread_mutex_init(&tree_lock, NULL);


    pthread_create(&thread0, NULL, thread_fn, (void *)&t0);
    pthread_create(&thread1, NULL, thread_fn, (void *)&t1);
    pthread_create(&thread2, NULL, thread_fn, (void *)&t2);

    pthread_create(&thread3, NULL, thread_fn, (void *)&t3);
    pthread_create(&thread4, NULL, thread_fn, (void *)&t4);
    pthread_create(&thread5, NULL, thread_fn, (void *)&t5);


    pthread_join(thread0, NULL);
    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(thread3, NULL);
    pthread_join(thread4, NULL);
    pthread_join(thread5, NULL);
    

    print_fg_bst(root);

    pthread_create(&thread0, NULL, delete_thread_fn, (void *)&t0);
    pthread_create(&thread1, NULL, delete_thread_fn, (void *)&t1);
    pthread_create(&thread2, NULL, delete_thread_fn, (void *)&t2);

    pthread_create(&thread3, NULL, delete_thread_fn, (void *)&t3);
    pthread_create(&thread4, NULL, delete_thread_fn, (void *)&t4);
    pthread_create(&thread5, NULL, delete_thread_fn, (void *)&t5);


    pthread_join(thread0, NULL);
    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(thread3, NULL);
    pthread_join(thread4, NULL);
    pthread_join(thread5, NULL);

    pthread_create(&thread0, NULL, sum_thread_fn, NULL);
    pthread_create(&thread1, NULL, sum_thread_fn, NULL);

    pthread_join(thread0, NULL);
    pthread_join(thread1, NULL);


    print_fg_bst(root);
    if (fg_is_BST(root) == 0) {
        printf("fuck\n");
    }
   
    return 0;
}

