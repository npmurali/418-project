/**
 * @file hash_set.h
 * 
 * @brief Library for a generic hash_setionary / hash table implementation. 
 * 
 * @author Namrita Murali
 * 
 * @bugs No known bugs. 
 */

#include <stdio.h>
#include <stdlib.h>
#include "hash_set.h"

/** @brief file private functions defined **/
int create_hash_set( hash_set_t *d, int size );
int grow_hash_set (hash_set_t *d);
int hash_function( int key );

/** Hash function for indexing into the hash_setionary */
int hash_function(int key){
    return key;
}

int hash_set_init( hash_set_t *d ){
    return create_hash_set(d, SET_ARR_SIZE);
}

int create_hash_set( hash_set_t *d, int size ){
    int i;
    if (size < 1) {
        return -1;
    }
    d->size = size;
    /** No elts in the hash_setionary */
    d->n  = 0;
    /** Create a hash table of linked lists of elts with
     * size given */
    d->table = malloc(sizeof(elt_t*) * d->size);
    if (d->table == NULL){
        return -1;
    }
    /** Initialize all linked lists to NULL */
    for (i = 0; i < d->size; i++){
        d->table[i] = NULL;
    }
    return 0;
}

int hash_set_reinit( hash_set_t *d ) {
    int i;
    int size = SET_ARR_SIZE;
    d->size = size;
    /** No elts in the hash_setionary */
    d->n  = 0;
    /** Create a hash table of linked lists of elts with
     * size given */
    d->table = realloc(d->table, sizeof(elt_t*) * d->size);
    if (d->table == NULL){
        return -1;
    }
    /** Initialize all linked lists to NULL */
    for (i = 0; i < d->size; i++){
        d->table[i] = NULL;
    }
    return 0;
}


int hash_set_destroy( hash_set_t *d ){
    int i;
    elt_t *e;
    elt_t *next;

    /** Free every element in every
     * linked list in the hash_setionary, and
     * get rid of each elements value with its destroy fn */
    for (i = 0; i < d->size; i++){
        for (e = d->table[i]; e != NULL; e = next){
            next = e->next;
            free(e);
        }
    }
    free(d->table);
    return 0;
}

int grow_hash_set( hash_set_t *d ){
    hash_set_t d2;
    hash_set_t temp_inside;
    int i;
    elt_t *e;

    /** Copy all the elements to the new hash_setionary of
     * size 2 * orig hash_set size and get rid of the new
     * hash_setionary */
    if ((create_hash_set(&d2, d->size * 2)) < 0){
        return -1;
    }

    for (i = 0; i < d->size; i++){
        for (e = d->table[i]; e != NULL; e = e->next){
            if (hash_set_insert(&d2, e->key) < 0) {
                return -1;
            }
        }
    }
    /** Move the pointer from the old hash_set to the new
     * hash_set and d2 to the old one so we can
     * destroy the old hash_setionary */
    temp_inside = *d;
    *d = d2;
    d2 = temp_inside;
    if (hash_set_destroy(&d2) < 0) {
        return -1;
    }
    return 0;
}

int hash_set_insert( hash_set_t *d, int key ){
    elt_t *e;
    unsigned int h;
    if (key < 0) {
        return -1;
    }
    e = malloc(sizeof(elt_t));
    if (e == NULL){
        return -1;
    }
    e->key = key;
    /** Insert new elements into the front of the
     * linked list of elements for that hash
     * function */
    int res = hash_function(key);
    h = hash_function(key) % d->size;
    res++;
    e->next = d->table[h];
    d->table[h] = e;
    /** Number of elements in the hash_setionary goes
     * up */
    d->n++;
    if (d->n >= d->size * MAX_LOAD_FACTOR){
        if ((grow_hash_set(d)) < 0) {
            return -1;
        }
    }
    return 0;
}

int hash_set_search( hash_set_t *d, int key ){
    elt_t *e;
    if (key < 0) {
        return 0;
    }
    for (e = d->table[hash_function(key) % d->size];
            e != NULL; e = e->next){
        if (e->key == key){
            return 1;
        }
    }
    return 0;
}

int hash_set_delete( hash_set_t *d, int key ){
    elt_t *e, *prev_e;
    int ret;
    if (key < 0) {
        return -1;
    }
    prev_e = e = d->table[hash_function(key) % d->size];
    /** If its the first element in the linked list entry,
     * make sure to change the pointer to the front
     * of the linked list */
    if (e->key == key){
        ret = e->key;
        d->table[hash_function(key) % d->size] = e->next;
        free(e);
        d->n--;
        return ret;
    }
    /** If its not the first element in linked list entry,,
     * find it and get rid of it in the linked list */
    e = e->next;
    while (e != NULL){
        if (e->key == key){
            ret = e->key;
            prev_e->next = e->next;
            free(e);
            d->n--;
            return ret;
        }
        prev_e = e;
        e = e->next;
    }
    /** return -1 because the element was not there **/
    return -1;
}

int hash_set_compare( hash_set_t *d1, hash_set_t *d2 ) {
    int i;
    elt_t *e;
    if (d1->size != d2->size) {
        return 0;
    }

    for (i = 0; i < d1->size; i++) {
        for (e = d1->table[i]; e != NULL; e = e->next) {
            if (hash_set_search(d2, e->key) == 0) {
                return 0;
            }
        }
    }
    return 1;
}


int hash_set_copy(hash_set_t *d1, hash_set_t *d2) {
    int i;
    elt_t *e;
    for (i = 0; i < d1->size; i++){
        for (e = d1->table[i]; e != NULL; e = e->next){
            if ((hash_set_insert(d2, e->key)) < 0) {
                return -1;
            }
        }
    }
    return 0;
}
