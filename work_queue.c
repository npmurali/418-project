/*
 * Work queue implementation
 */

#include "work_queue.h"
#include <stdlib.h>
#include <stdio.h>

void wq_init( wq_t *queue ) {
    queue->head = NULL;
    queue->tail = NULL;
    queue->size = 0;
    pthread_mutex_init(&queue->queue_lock, NULL);
}

int wq_enq( wq_t *queue, wq_op_t op, int value ) {
    wq_item_t *item;

    item = (wq_item_t *)malloc(sizeof(wq_item_t));
    if (item == NULL) {
        fprintf(stderr, "ERROR allocating memory\n");
        return -1;
    }

    item->operation = op;
    item->value = value;

    // first item in the work queue
    if (queue->head == NULL) {
        queue->head = item;
    }
    // otherwise, add to end of queue
    else {
        queue->tail->next = item;
    }
    // point tail to new item added
    queue->tail = item;
    queue->size++;
    queue->tail->next = NULL;
    return 0;
}

int wq_deq( wq_t *queue, wq_item_t *item ) {

    wq_item_t *temp;

    // trying to deq an empty queue
    if (queue->head == NULL) {
        return -1;
    }

    temp = queue->head;
    if (item != NULL) {
        // update our value to the head
        item->operation = temp->operation;
        item->value = temp->value;
        item->next = NULL;
    }

    queue->head = queue->head->next;
    // taking out last element
    if (queue->head == queue->tail) {
        queue->tail = NULL;
    }
    queue->size--;
    free(temp);
    return 0;
}

void wq_destroy( wq_t *queue ) {

    while (queue->head != NULL) {
        wq_deq(queue, NULL);
    }
}

int wq_is_empty( wq_t *queue ) {
    int result;
    //pthread_mutex_lock(&queue->queue_lock);
    result = (queue->head == NULL);
    //pthread_mutex_unlock(&queue->queue_lock);
    return result;
}

void print_wq( wq_t *queue ) {
    wq_item_t *item;

    int i = 0;
    item = queue->head;
    while (item != NULL) {
        printf("| %d | op: %d | value: %d |\n", i, item->operation, item->value);
        i++;
        item = item->next;
    }
}





