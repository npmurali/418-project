/**
 * Sequential BST implementation
 */

#include "seq_BST.h"
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

seq_node_t *seq_BST_insert( seq_node_t *root, int value ) {

    // empty tree
    if (root == NULL) {
        root = (seq_node_t *)malloc(sizeof(seq_node_t));
        root->left = NULL;
        root->right = NULL;
        root->value = value;
        return root;
    }
    
    // insert into left subtree
    else if (value < root->value) {
        root->left = seq_BST_insert(root->left, value);
    }
    // insert into right subtree
    else if (value > root->value) {
        root->right = seq_BST_insert(root->right, value);
    }
    return root;
}

seq_node_t *seq_BST_search( seq_node_t *root, int value ) {
    // empty tree or value at root
    if (root == NULL || root->value == value) {
        return root;
    }

    // search left subtree
    if (value < root->value) {
        return seq_BST_search(root->left, value);
    }
    // search right subtree
    return seq_BST_search(root->right, value);
}

/*seq_node_t *seq_BST_delete( seq_node_t *root, int value ) {

    // empty tree
    if (root == NULL) {
        return root;
    }

    // val lies in left subtree
    if (value < root->value) {
        root->left = seq_BST_delete(root->left, value);
    }
    // val lies in right subtree
    else if (value > root->value) {
        root->right = seq_BST_delete(root->right, value);
    }
    // value is current node's value
    else {
        // only one child
        if (root->left == NULL) {
            seq_node_t *temp = root->right;
            free(root);
            return temp;
        }
        else if (root->right == NULL) {
            seq_node_t *temp = root->left;
            free(root);
            return temp;
        }
        else {
            // find smallest value 
            // greater than current node
            seq_node_t *temp = root->right;
            while (temp->left != NULL) {
                temp = temp->left;
            }
            root->value = temp->value;
            root->right = seq_BST_delete(root->right, temp->value);
        }
    }
    return root;
}*/

seq_node_t *delete_root_node(seq_node_t *root) {
    if (root == NULL) {
        return NULL;
    }
    if (root->left == NULL) {
        return root->right;
    }
    if (root->right == NULL) {
        return root->left;
    }

    seq_node_t *temp = root->right;
    seq_node_t *parent = NULL;

    while (temp->left != NULL) {
        parent = temp;
        temp = temp->left;
    }
    temp->left = root->left;
    if (root->right != temp) {
        parent->left = temp->right;
        temp->right = root->right;
    }
    return temp;
}

seq_node_t *seq_BST_delete( seq_node_t *root, int value ) {
    seq_node_t *node, *parent;

    node = root;
    parent = NULL;

    while (node != NULL && node->value != value) {
        parent = node;
        if (value < node->value) {
            node = node->left;
        }
        else if (value > node->value) {
            node = node->right;
        }
    }
    if (parent == NULL) {
        return delete_root_node(node);
    }
    if (parent->left == node) {
        parent->left = delete_root_node(node);
    }
    else {
        parent->right = delete_root_node(node);
    }
    return root;
}


int seq_BST_sum( seq_node_t *root ) {
    // empty tree
    if (root == NULL) {
        return 0;
    }
    return (root->value + seq_BST_sum(root->left) + seq_BST_sum(root->right));
}

int seq_is_BST_helper( seq_node_t *root, int min, int max ) {
    // emptry tree is valid
    if (root == NULL) {
        return 1;
    }

    if (root->value < min || root->value > max) {
        return 0;
    }

    // recursive check tightening constraints
    return (seq_is_BST_helper(root->left, min, root->value - 1)) &&
           (seq_is_BST_helper(root->right, root->value + 1, max));
}

int seq_is_BST( seq_node_t *root ) {
    return seq_is_BST_helper(root, INT_MIN, INT_MAX);
}    

void print_seq_bst( seq_node_t *root ) {
    if (root != NULL) {
        printf("%d \n", root->value);
        print_seq_bst(root->left);
        print_seq_bst(root->right);
    }
}

void seq_bst_to_hash_set( seq_node_t *root, hash_set_t *set ) {
    if (hash_set_reinit(set) < 0) {
        return;
    }

    if (root != NULL) {
        hash_set_insert(set, root->value);
        seq_bst_to_hash_set(root->left, set);
        seq_bst_to_hash_set(root->right, set);
    }
    return;
}
