#ifndef __FG_BST_H
#define __FG_BST_H
/**
 * Fine-grained Locking Concurrent Binary Search Tree
 */

#include <stdlib.h>
#include <pthread.h>
#include "hash_set.h"

/*
 * Node data structure
 */
typedef struct fg_node_t {
    struct fg_node_t *left;
    struct fg_node_t *right;
    int value;
    pthread_mutex_t node_lock;
} fg_node_t;

typedef struct fg_tree_t {
    struct fg_node_t *root;
    pthread_mutex_t tree_lock;
} fg_tree_t;


/*
 * Global tree lock
 */
pthread_mutex_t tree_lock;

fg_node_t *tree_top;

/*
 * Insert operation
 */
//fg_node_t *fg_BST_insert( fg_tree_t *fg_tree_top, fg_node_t *root, fg_node_t *parent, int value );
fg_node_t *fg_BST_insert( fg_node_t *root, fg_node_t *parent, int value);

/*
 * Search operation
 */
fg_node_t *fg_BST_search( fg_node_t *root, fg_node_t *parent, int value );

/*
 * Delete operation
 */
//void fg_BST_delete( fg_tree_t * fg_tree_top,  int value );
fg_node_t *fg_BST_delete( fg_node_t *root, fg_node_t *parent, int value);

/*
 * Sum operation
 */
int fg_BST_sum( fg_node_t *root, fg_node_t *parent );

/**
 * Validator
 */
int fg_is_BST( fg_node_t *root );

/**
 * Debugging print function
 */
void print_fg_bst( fg_node_t *root );

void fg_bst_to_hash_set( fg_node_t *root, hash_set_t *set );

#endif
