/**
 * Lock free Binary search tree implementation
 */

#include "lf_BST.h"
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#define TAG (0b01)
#define FLAG (0b10)


#define ADDR(x) ((lf_node_t *)((((unsigned long)(x)) & (~(TAG | FLAG)))))
#define LEFT(x) ((ADDR(x))->left)
#define RIGHT(x) ((ADDR(x))->right)

#define IS_TAGGED(x) (((unsigned long)(x)) == (((unsigned long)(x)) | TAG))
#define IS_FLAGGED(x) (((unsigned long)(x)) == (((unsigned long)(x)) | FLAG))

#define SET_FLAG(x) ((lf_node_t *)(((unsigned long)(x)) | FLAG))

#define UNTAG(x) ((lf_node_t *)(((unsigned long)(x)) & ~TAG))

typedef enum deletion_mode_t {INJECTION, CLEANUP} deletion_mode_t;
typedef enum inf_t {INF_2 = INT_MAX, INF_1 = INT_MAX - 1, INF_0 = INT_MAX -2} inf_t;

/*
 * seek record
 */
typedef struct lf_seek_record_t {
    lf_node_t *leaf;
    lf_node_t *parent;
    lf_node_t *ancestor;
    lf_node_t *successor;
} lf_seek_record_t;


lf_node_t *lf_BST_init( void ) {
    lf_node_t *root;
    root = (lf_node_t *)malloc(sizeof(lf_node_t));
    root->value = INF_2;

    root->right = (lf_node_t *)malloc(sizeof(lf_node_t));
    root->right->value = INF_2;;
    root->right->left = NULL;
    root->right->right = NULL;


    root->left = (lf_node_t *)malloc(sizeof(lf_node_t));
    root->left->value = INF_1;

    root->left->left = (lf_node_t *)malloc(sizeof(lf_node_t));
    root->left->left->value = INF_0;
    root->left->left->left = NULL;
    root->left->left->right = NULL;

    root->left->right = (lf_node_t *)malloc(sizeof(lf_node_t));
    root->left->right->value = INT_MAX - 1;
    root->left->right->left = NULL;
    root->left->right->right = NULL;
    return root;
}

/*
 * Seek operation
 */
void lf_seek( lf_node_t *root, lf_seek_record_t *seek_record, int value ) {
    lf_node_t *S = root;

    seek_record->ancestor = root;
    seek_record->successor = S;
    seek_record->parent = S;
    seek_record->leaf = ADDR(LEFT(S));

    lf_node_t *parent_field, *current_field;
    parent_field = LEFT(seek_record->parent);
    current_field = LEFT(seek_record->leaf);
    lf_node_t *current = ADDR(current_field);

    while (current != NULL) {
        // move down the tree
        // check if the edge from the current parent node in the access path is tagged
        if (!IS_TAGGED(parent_field)) {
            seek_record->ancestor = seek_record->parent;
            seek_record->successor = seek_record->leaf;
        }

        seek_record->parent = seek_record->leaf;
        seek_record->leaf = current;

        parent_field = current_field;
        if (value < current->value) {
            current_field = LEFT(current);
        }
        else {
            current_field = RIGHT(current);
        }
        current = ADDR(current_field);
    }
}
    

/*
 * Cleanup operation
 */

int cleanup( lf_seek_record_t *seek_record, int value ) {
    lf_node_t *ancestor = seek_record->ancestor;
    lf_node_t *successor = seek_record->successor;
    lf_node_t *parent = seek_record->parent;

    lf_node_t **successor_addr, **sibling_addr, **child_addr;

    if (value < ancestor->value) {
        successor_addr = &(LEFT(ancestor));
    }
    else {
        successor_addr = &(RIGHT(ancestor));
    }

    // now obtain the addrs of hte child fields of the parent node
    if (value < parent->value) {
        child_addr = &(parent->left);
        sibling_addr = &(parent->right);
    }
    else {
        child_addr = &(parent->right);
        sibling_addr = &(parent->left);
    }
    if (!(IS_FLAGGED(*child_addr))) {
        sibling_addr = child_addr;
    }
    if (__sync_fetch_and_or(sibling_addr, TAG)) {

        if (__sync_bool_compare_and_swap(successor_addr, ADDR(successor), UNTAG(*sibling_addr))) {
               return 1;
        }
    }
    return 0;
}


 
/*
 * Insert operation
 */
int lf_BST_insert( lf_node_t *root, int value ) {
    
    lf_seek_record_t *seek_record = (lf_seek_record_t *)malloc(sizeof(lf_seek_record_t));

    while (1) {
        lf_seek(root, seek_record, value);

        if (seek_record->leaf->value != value || seek_record->leaf == NULL) {
            // key not present in tree
            lf_node_t *parent = seek_record->parent;
            lf_node_t *leaf = seek_record->leaf;

            lf_node_t **child_addr;

            if (value < parent->value) {
                child_addr = &(parent->left);
            }
            else {
                child_addr = &(parent->right);
            }

            // create new internal and leaf nodes and initialize them
            lf_node_t *new_leaf = (lf_node_t *)malloc(sizeof(lf_node_t));
            lf_node_t *new_internal = (lf_node_t *)malloc(sizeof(lf_node_t));

            new_leaf->value = value;
            new_leaf->left = NULL;
            new_leaf->right = NULL;
            if (leaf->value < value) {
                new_internal->value = value;
                new_internal->left = leaf;
                new_internal->right = new_leaf;
            }
            else {
                new_internal->value = leaf->value;
                new_internal->left = new_leaf;
                new_internal->right = leaf;
            }

            // try to add the new nodes to the tree
            if (__sync_bool_compare_and_swap(child_addr, leaf, new_internal)) {
                    return 1;
            }
            else {
                free(new_leaf);
                free(new_internal);

                lf_node_t *addr = ADDR(child_addr);
                if ((addr == leaf) && (IS_TAGGED(child_addr) || IS_FLAGGED(child_addr))) {
                    cleanup(seek_record, value);
                }
            }
        }
        else {
            return 0;
        }
    }
}

/*
 * Search operation
 */
int lf_BST_search( lf_node_t *root, int value ) {
    lf_seek_record_t *seek_record = (lf_seek_record_t *)malloc(sizeof(lf_seek_record_t));
    lf_seek(root, seek_record, value);

    if (seek_record->leaf->value == value) {
        free(seek_record);
        return 1;
    }
    free(seek_record);
    return 0;
}

/*
 * Delete operation
 */
int lf_BST_delete( lf_node_t *root, int value ) {

    deletion_mode_t mode = INJECTION;
    int result, done;

    while (1) {
        lf_seek_record_t *seek_record = (lf_seek_record_t *)malloc(sizeof(lf_seek_record_t));
        lf_seek(root, seek_record, value);
        lf_node_t *parent = seek_record->parent;
        lf_node_t **child_addr;
        lf_node_t *leaf;

        // obtain addr of child that needs to be modified
        if (value < parent->value) {
            child_addr = &(parent->left);
        }
        else {
            child_addr = &(parent->right);
        }
        if (mode == INJECTION) {
            leaf = seek_record->leaf;
            if (leaf->value != value) {
                // value not present in tree
                return 0;
            }
            // inject delete operation in tree
            result = __sync_bool_compare_and_swap(child_addr, ADDR(leaf), SET_FLAG(UNTAG(leaf)));
            if (result) {
                mode = CLEANUP;
                done = cleanup(seek_record, value);
                if (done) {
                    return 1;
                }
            }
            else {
                if (ADDR(child_addr) && (IS_FLAGGED(child_addr) || IS_TAGGED(child_addr))) {
                    cleanup(seek_record, value);
                }
            }
        }
        else {
            if (seek_record->leaf != leaf) {
                return 1;
            }
            else {
                done = cleanup(seek_record, value);
                if (done) {
                    return 1;
                }
            }
        }
        
    }
}

/*
 * Sum operation
 */
int lf_BST_sum( lf_node_t *root ) {
    if (root != NULL) {
        if (root->left == NULL && root->right == NULL) {
            if (root->value != INF_2 &&
                root->value != INF_1 &&
                root->value != INF_0) {
                return root->value;
            }
        }
        else {
            return (lf_BST_sum(root->left) + lf_BST_sum(root->right));
        }
    }
    return 0;
}

/**
 * Validator
 */
int lf_is_BST( lf_node_t *root ) {
    return 0;
}

/**
 * Debugging print function
 */
void print_lf_bst( lf_node_t *root ) {
    if (root != NULL) {
        if (root->left == NULL && root->right == NULL) {
            printf("%d \n", root->value);
        }

        print_lf_bst(root->left);
        print_lf_bst(root->right);
    }
}

void lf_bst_to_hash_set( lf_node_t *root, hash_set_t *set ) {

    if (hash_set_reinit(set) < 0) {
        return;
    }
    if (root != NULL) {
        if (root->left == NULL && root->right == NULL) {
            if (root->value != INF_2 &&
                root->value != INF_1 &&
                root->value != INF_0) {
                hash_set_insert(set, root->value);
            }
        }
        lf_bst_to_hash_set(root->left, set);
        lf_bst_to_hash_set(root->right, set);
    }
    return;
}

